#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

def ensure_container(a):
    if not (isinstance(a, tuple) or isinstance(a, list)):
        return (a,)
    else:
        return a

def call_function_get_frame(func, *args, **kwargs):
    """
    Calls the function *func* with the specified arguments and keyword
    arguments and snatches its local frame before it actually executes.
    """

    frame = None
    trace = sys.gettrace()
    def snatch_locals(_frame, name, arg):
      nonlocal frame
      if frame is None and name == 'call':
        frame = _frame
        sys.settrace(trace)
      return trace
    sys.settrace(snatch_locals)
    try:
      result = func(*args, **kwargs)
    finally:
      sys.settrace(trace)

    return frame, result
