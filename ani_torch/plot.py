#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from collections import defaultdict
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

def format_labels(label_dict, precision=3):
    return {k: np.array2string(np.asarray(v), precision=precision)
        if not isinstance(v, str) else v for k,v in label_dict.items()}

def update_move(move_up, move_left, ax):
    ax_lims = ax.axis()
    move_up = move_up * (ax_lims[3] - ax_lims[2])    
    move_left = move_left * (ax_lims[1] - ax_lims[0])
    return move_up, move_left

def update_margins(ax_margins, move_up, move_left):
    if not ax_margins is None:
        if move_up > 0:
            ax_margins[3] = max(ax_margins[3], move_up)
        else:
            ax_margins[2] = max(ax_margins[2], -move_up)
    
        if move_left > 0:
            ax_margins[0] = max(ax_margins[0], move_left)
        else:
            ax_margins[1] = max(ax_margins[0], -move_left)

def do_axlims_update(ax, ax_margins):
    axlims = ax.axis()
    axlims = [axlims[0] - ax_margins[0], axlims[1] + ax_margins[1],
              axlims[2] - ax_margins[2], axlims[3] + ax_margins[3]]
    ax.axis(axlims)

def draw_node_labels(G, pos, labels,
                     move_up=0, move_left=0,
                     rotate=False, font_color='k',
                     label_format_func=None, format_precision=None,
                     ax=None, ax_margins=None, **kwargs):
    if ax is None:
        ax = plt.gca()

    if label_format_func is None:
        if format_precision is None:
            label_format_func = format_labels
        else:
            label_format_func = lambda l: format_labels(l, precision=format_precision)

    move_up, move_left = update_move(move_up, move_left, ax)
    update_margins(ax_margins, move_up, move_left)

    if not label_format_func is None:
        labels = label_format_func(labels)

    nx.draw_networkx_labels(
        G, {n: (p[0]-move_left, p[1]+move_up) for n,p in pos.items()},
        labels=labels,
        # rotate=rotate,
        font_color=font_color,
        ax=ax,
        **kwargs
    )

def draw_edge_labels(G, pos, edge_labels,
                     move_up=0, move_left=0,
                     label_pos=0.5, rotate=False,
                     font_color='k',
                     label_format_func=None,
                     format_precision=None,
                     ax=None, ax_margins=None,
                     **kwargs):
    if ax is None:
        ax = plt.gca()

    if label_format_func is None:
        if format_precision is None:
            label_format_func = format_labels
        else:
            label_format_func = lambda l: format_labels(l, precision=format_precision)

    move_up, move_left = update_move(move_up, move_left, ax)
    update_margins(ax_margins, move_up, move_left)    

    if not label_format_func is None:
        edge_labels = label_format_func(edge_labels)

    nx.draw_networkx_edge_labels(
        G, {n: (p[0]-move_left, p[1]+move_up) for n,p in pos.items()},
        edge_labels=edge_labels,
        label_pos=label_pos,
        # rotate=rotate,
        font_color=font_color,
        ax=ax,
        **kwargs
    )

def graph_plot(G, pos,
    node_size=1000, node_linewidth=2,
    edge_linewidth=2, label_format_func=None,
    ax=None, ax_margins=None, node_kwargs=dict(),
    edge_kwargs=dict(), node_label_kwargs=dict()
):
    if ax is None:
        ax = plt.gca()

#    # make sure that no nodes are cropped
#    marg = node_size / 2 / ax.get_figure().dpi
#    ax_margins[0] = max(ax_margins[0], marg)
#    ax_margins[1] = max(ax_margins[1], marg)
#    ax_margins[2] = max(ax_margins[2], marg)
#    ax_margins[3] = max(ax_margins[3], marg)

    linewidths = []

    for node, attr in G.nodes(data=True):
        if attr["is_input"] or attr["is_output"]:
            linewidths.append(0)
        else:
            linewidths.append(node_linewidth)

    nx.draw_networkx_nodes(
        G, pos, node_size=node_size, node_color='w',
        edgecolors='k', linewidths=linewidths, ax=ax,
        **node_kwargs
    )

    nx.draw_networkx_edges(
        G, pos, width=edge_linewidth, arrowsize=30,
        node_size=0.9*node_size, ax=ax,
        **edge_kwargs
    )

    node_labels = nx.get_node_attributes(G, 'label')
    
    if not label_format_func is None:
        node_labels = {k: label_format_func(v) for k, v in node_labels}

    draw_node_labels(G, pos,
        labels=node_labels, ax=ax,
        **node_label_kwargs)

    ax.axis('off')

def sort_nodes(G, direction):
    if direction == "forward":
        sorted_nodes = list(nx.topological_sort(G))
    elif direction == "backward":
        sorted_nodes = list(nx.topological_sort(G))
        sorted_nodes.reverse()
    else:
        raise ValueError("Arg. direction must be either 'forward' or 'backward'.")
        
    return sorted_nodes

def edge_labels_by_node(edge_labels, direction):
    if direction == "forward":
        edge_idx = 0
    elif direction == "backward":
        edge_idx = 1
    else:
        raise ValueError("Arg. direction must be either 'forward' or 'backward'.")
        
    labels_by_node = defaultdict(list)
    
    for edge, out in edge_labels.items():
        labels_by_node[edge[edge_idx]].append((edge, out))
    
    labels_by_node = dict(labels_by_node)
    return labels_by_node

def edge_label_animate(G, edge_labels, direction):
    sorted_nodes = sort_nodes(G, direction)
    labels_by_node = edge_labels_by_node(edge_labels, direction)
    
    for inode, node in enumerate(sorted_nodes):
        step_edge_labels = {}
        
        try:
            for edge, out in labels_by_node[node]:
                step_edge_labels[edge] = out

            if len(labels_by_node[node]):
                yield step_edge_labels
        except KeyError:
            continue

