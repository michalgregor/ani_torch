#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import re
import torch
import networkx as nx
from IPython.display import HTML
from IPython.utils.capture import capture_output
from matplotlib import animation
from collections import deque
from ._tracking import TensorTracker, make_tensor_names, TrackingFuncWrapper, ext_inner_tensor
from .node_names import _default_node_name_dict
from .utils import ensure_container, call_function_get_frame
from .plot import *
import itertools

_backward_name_regex = re.compile("Backward[0-9]*")

def _make_graph(outputs, tensor_list=None, tensor_names=None,
               backward_callback=None, node_name_dict=_default_node_name_dict):
    if tensor_names is None:
        tensor_names = {}
        
    if tensor_list is None:
        tensor_dict = {}
    else:
        tensor_dict = {t.grad_fn:t for t in tensor_list if not t.grad_fn is None}
    
    G = nx.MultiDiGraph()
    
    to_expand = deque()
    expanded = set()
    untracked = []
    
    for iout, t in enumerate(set(outputs)):
        t = ext_inner_tensor(t)
        to_expand.appendleft(t.grad_fn)
        
        try:
            t_label = tensor_names[t]
        except KeyError:
            t_label = "out_{}".format(iout)
        
        G.add_node(t, label=t_label,
                   is_input=False, is_output=True)
        G.add_edge(t.grad_fn, t, tensor=t, out_id=t.output_nr, inp_id=0)
    
    while len(to_expand):
        node = to_expand.pop()
        graph_node = node
        
        if not node in expanded:
            expanded.add(node)
            node_label = None
            is_input = False
            
            if not len(node.next_functions):
                is_input = True
                
                try:
                    node_label = tensor_names[node.variable]
                except KeyError:
                    node_label = node.variable.detach().numpy()               
                
                graph_node = node.variable
            else:
                for inp_id, n in enumerate(node.next_functions):
                    src_node = n[0]
                    out_id = n[1]
                    
                    if not src_node is None:
                        to_expand.appendleft(src_node)
                        if not len(src_node.next_functions):
                            src_node = src_node.variable
                        
                        try:
                            edge_tensor = tensor_dict[src_node]

                            try:
                                edge_tensor_name = tensor_names[edge_tensor]
                            except KeyError:
                                edge_tensor_name = None

                        except KeyError:
                            edge_tensor = None
                            edge_tensor_name = None
                            
                        G.add_edge(src_node, node, tensor=edge_tensor,
                                   out_id=out_id, inp_id=inp_id,
                                   tensor_name=edge_tensor_name)
                    else:
                        unt = "??_{}".format(len(untracked))
                        untracked.append(unt)
                        G.add_node(unt, label=unt,
                                   is_input=True, is_output=False)
                        G.add_edge(unt, node, tensor=None,
                                   out_id=out_id, inp_id=inp_id)

            if node_label is None:
                node_label = node.__class__.__name__
                
                try:
                    node_label = node_name_dict[node_label]
                except KeyError:
                    if hasattr(node, '_forward_cls') and hasattr(node._forward_cls, 'name'):
                        node_label = node._forward_cls.name
                    else:
                        node_label = _backward_name_regex.sub('', node_label).lower()
                
            if not backward_callback is None:
                node.register_hook(lambda in_grad, out_grad, node=node, node_label=node_label:
                   backward_callback((in_grad, out_grad, node, node_label)))
                    
            G.add_node(graph_node, label=node_label,
                       is_input=is_input, is_output=False)
            
            if hasattr(node, 'variable'):
                for e in G.edges((node.variable,), data=True):
                    e[2]['tensor'] = node.variable

    return G

def _collect_edge_outputs(G, tensor_list):
    edge_outputs = {}
    
    for it, t in enumerate(tensor_list):
        src = t if t.grad_fn is None else t.grad_fn
        
        for edge in G.edges((src,)):
            edge_outputs[edge] = t.detach().numpy()
            
    return edge_outputs

def _collect_grads(G, backward_gate_list):
    gate_grad_dict = {it[2]: (it[0], it[1]) for it in backward_gate_list}
    edge_grads = {}
    input_grads = {}

    for fr, to, data in G.edges(data=True):
        if isinstance(to, torch.Tensor):        
            edge_grads[(fr, to)] = gate_grad_dict[fr][1][to.output_nr].detach().numpy()
        else:
            edge_grads[(fr, to)] = gate_grad_dict[to][0][data['inp_id']].detach().numpy()

        if isinstance(fr, torch.Tensor):
            input_grads[fr] = fr.grad.detach().numpy()
            
    return edge_grads, input_grads

def patched_graphviz_layout(G):
    node2idx = {}
    idx2node = []

    for inode, node in enumerate(G.nodes()):
        node2idx[node] = inode
        idx2node.append(node)

    G_relabeled = nx.relabel_nodes(G, node2idx, copy=True)
    pos = nx.drawing.nx_agraph.graphviz_layout(G_relabeled, prog='dot')
    pos = {idx2node[k]:v for k,v in pos.items()}
    
    return pos

class GraphAnimation:
    def __init__(
        self,
        graph,
        direction="forward", # forward, backward, both
        ax=None,
        ax_margins=None,
        update_axlims=True,
        num_label_format=None,
        num_label_precision=None,
        # forward and backward kwargs
        with_all=False,
        with_forward=True,
        forward_kwargs=None,
        forward_step_kwargs=None,
        backward_kwargs=None,
        backward_step_kwargs=None,
        # input_grad
        with_in_grad=True,
        in_grad_kwargs=None,
        # intermediate
        with_intermediate=False,
        intermediate_kwargs=None,
        # other kwargs
        graph_kwargs = {}
    ):
        if not direction in ["forward", "backward", "both"]:
            raise ValueError("Unknown direction '{}'.".format(direction))

        self.graph = graph

        with capture_output() as io:
            self.fig = plt.gcf()
            self.ax = ax or plt.gca()
            plt.show()

        self.direction = direction
        self.ax_margins = ax_margins or [0, 0, 0, 0]
        self.update_axlims = update_axlims
        self.num_label_format = num_label_format
        self.num_label_precision = num_label_precision
        
        if with_all:
            with_forward = True
            with_in_grad = True

        self.with_forward = with_forward
        self.with_in_grad = with_in_grad
        self.with_intermediate = with_intermediate
        
        forward_kwargs = forward_kwargs or {}
        self.forward_kwargs = dict(TorchGraph._default_forward_kwargs, **forward_kwargs)
        
        forward_step_kwargs = forward_step_kwargs or {}
        self.forward_step_kwargs = dict(self.forward_kwargs, **TorchGraph._default_forward_step_kwargs)
        self.forward_step_kwargs.update(**forward_step_kwargs)
        
        backward_kwargs = backward_kwargs or {}
        self.backward_kwargs = dict(TorchGraph._default_backward_kwargs, **backward_kwargs)
        
        backward_step_kwargs = backward_step_kwargs or {}
        self.backward_step_kwargs = dict(self.backward_kwargs, **TorchGraph._default_backward_step_kwargs)
        self.backward_step_kwargs.update(**backward_step_kwargs)
                                        
        in_grad_kwargs = in_grad_kwargs or {}
        self.in_grad_kwargs = dict(TorchGraph._default_in_grad_kwargs, **in_grad_kwargs)

        intermediate_kwargs = intermediate_kwargs or {}
        self.intermediate_kwargs = dict(TorchGraph._default_intermediate_kwargs, **intermediate_kwargs)

        self.graph_kwargs = graph_kwargs         

    def _plot_graph(self, direction, ax_margins, update_axlims):
        self.graph.plot(
            ax_margins=ax_margins,
            update_axlims=update_axlims,
            ax=self.ax,
            with_forward=self.with_forward and (direction != "forward"),
            with_backward=False,
            with_in_grad=False,
            forward_kwargs=self.forward_kwargs,
            num_label_format=self.num_label_format,
            num_label_precision=self.num_label_precision,
            with_intermediate=self.with_intermediate,
            intermediate_kwargs=self.intermediate_kwargs,
            **self.graph_kwargs
        )

    def _make_ani_generate(self, direction, ani_kwargs, ani_step_kwargs, ani_generator):
        prev_l = {}

        # only plot the graph itself in the first step
        ax_margins = list(self.ax_margins)
        self.ax.clear()
        self._plot_graph(direction, ax_margins, update_axlims=self.update_axlims)
        yield

        # then add more stuff step by step
        for cur_l in ani_generator:
            ax_margins = list(self.ax_margins)

            self.ax.clear()
            self._plot_graph(direction, ax_margins, update_axlims=False)

            draw_edge_labels(self.graph.G, self.graph.pos, prev_l, ax=self.ax,
                             label_format_func=self.num_label_format,
                             format_precision=self.num_label_precision,
                             ax_margins=self.ax_margins,
                             **ani_kwargs)
            draw_edge_labels(self.graph.G, self.graph.pos, cur_l, ax=self.ax,
                             label_format_func=self.num_label_format,
                             format_precision=self.num_label_precision,
                             ax_margins=self.ax_margins,
                             **ani_step_kwargs)

            if self.update_axlims:
                do_axlims_update(self.ax, self.ax_margins)

            prev_l.update(cur_l)
            yield

        if self.with_in_grad and direction != "forward":
            self.ax.clear()
            self._plot_graph(direction, ax_margins, update_axlims=False)

            draw_edge_labels(self.graph.G, self.graph.pos, prev_l, ax=self.ax,
                             label_format_func=self.num_label_format,
                             format_precision=self.num_label_precision,
                             ax_margins=self.ax_margins,
                             **ani_kwargs)

            draw_node_labels(
                 self.graph.G, self.graph.pos,
                 self.graph.input_grads, ax=self.ax,
                 label_format_func=self.num_label_format,
                 ax_margins=self.ax_margins, format_precision=self.num_label_precision,
                 **self.in_grad_kwargs)

            if self.update_axlims:
                do_axlims_update(self.ax, self.ax_margins)

            yield
        
    def __call__(self):
        if self.direction == "forward":
            ani_kwargs = self.forward_kwargs
            ani_step_kwargs = self.forward_step_kwargs
            ani_generator = edge_label_animate(self.graph.G, self.graph.edge_outputs, "forward")
            return self._make_ani_generate("forward", ani_kwargs, ani_step_kwargs, ani_generator)
        elif self.direction == "backward":
            ani_kwargs = self.backward_kwargs
            ani_step_kwargs = self.backward_step_kwargs
            ani_generator = edge_label_animate(self.graph.G, self.graph.edge_grads, "backward")
            return self._make_ani_generate("backward", ani_kwargs, ani_step_kwargs, ani_generator)
        else:
            ani_generator = edge_label_animate(self.graph.G, self.graph.edge_outputs, "forward")
            forward_gen = self._make_ani_generate(
                "forward", self.forward_kwargs, self.forward_step_kwargs, ani_generator)

            ani_generator = edge_label_animate(self.graph.G, self.graph.edge_grads, "backward")
            backward_gen = self._make_ani_generate(
                "backward", self.backward_kwargs,  self.backward_step_kwargs, ani_generator)

            return itertools.chain(forward_gen, backward_gen)

class TorchGraph:
    _default_forward_kwargs = dict(
        font_color='g'
    )

    _default_forward_step_kwargs = dict()

    _default_backward_kwargs = dict(
        font_color='r',
        move_up=0.1
    )

    _default_backward_step_kwargs = dict(
        bbox=dict(boxstyle='round', ec='r', fc='w', pad=0.6)
    )

    _default_in_grad_kwargs = dict(
        font_color='r',
        move_up=-0.06,
        move_left=0.02
    )

    _default_intermediate_kwargs = dict(
        move_up=-0.08,
        move_left=0.02
    )

    def __init__(self, torch_func, inputs, output_grads=None,
                 rankdir='LR', layout_func=None):
        self.torch_func = torch_func
        self.inputs = inputs
        self.output_grads = output_grads
        self._compile(rankdir, layout_func=None)

    def _compile(self, rankdir='LR', layout_func=None):
        if layout_func is None:
            layout_func = patched_graphviz_layout

        tensor_list = []
        self.tensor_names = []
        backward_gate_list = []

        with TensorTracker(tensor_list.append, self.tensor_names.append):
            input_tensors = [
                torch.tensor(x, dtype=torch.float32, requires_grad=True)
                    for x in self.inputs
            ]

            frame, self.outputs = call_function_get_frame(
                self.torch_func, *input_tensors
            )

        self.outputs = ensure_container(self.outputs)
        make_tensor_names(frame.f_locals, self.tensor_names.append)

        self.G = _make_graph(
            self.outputs, tensor_list,
            {v:k for k, v in self.tensor_names},
            backward_callback=backward_gate_list.append
        )
        self.G.graph['graph'] = {'rankdir': rankdir}

        if self.output_grads:
            output_grad_tensors = [
                torch.tensor(g, dtype=torch.float32, requires_grad=True)
                    for g in ensure_container(self.output_grads)
            ]

            if len(output_grad_tensors) != len(self.outputs):
                if len(output_grad_tensors) == 1:
                    output_grad_tensors = [output_grad_tensors for out in self.outputs]
                else:
                    raise ValueError("The number of output gradients differs from the number of outputs.")
        else:
            output_grad_tensors = [None for out in self.outputs]

        for out, grad in zip(self.outputs, output_grad_tensors):
            out.backward(grad)

        self.edge_outputs = _collect_edge_outputs(self.G, tensor_list)
        self.edge_grads, self.input_grads = _collect_grads(self.G, backward_gate_list)
        self.pos = layout_func(self.G)
        self.intermediate_labels = {
            (k[0], k[1]): v for k,v
             in nx.get_edge_attributes(self.G, "tensor_name").items()
                if not v is None
        }

    @property
    def rankdir(self):
        return self.G.graph['graph']['rankdir']

    @rankdir.setter
    def rankdir(self, rankdir):
        self.G.graph['graph']['rankdir'] = rankdir

    def plot(self,
        # graph
        graph_kwargs=dict(),
        node_kwargs=dict(),
        edge_kwargs=dict(),
        node_label_kwargs=dict(),
        num_label_format=None,
        num_label_precision=None,
        ax=None,
        ax_margins=None,
        update_axlims=True,
        with_all=False,
        # forward
        with_forward=False,
        forward_kwargs=None,
        # backward
        with_backward=False,
        backward_kwargs=None,
        # input_grad
        with_in_grad=False,
        in_grad_kwargs=None,
        # intermediate
        with_intermediate=False,
        intermediate_kwargs=None
    ):
        if ax is None:
            ax = plt.gca()

        if ax_margins is None:
            ax_margins = [0, 0, 0, 0]

        forward_kwargs = forward_kwargs or {}
        forward_kwargs = dict(TorchGraph._default_forward_kwargs, **forward_kwargs)
        backward_kwargs = backward_kwargs or {}
        backward_kwargs = dict(TorchGraph._default_backward_kwargs, **backward_kwargs)
        in_grad_kwargs = in_grad_kwargs or {}
        in_grad_kwargs = dict(TorchGraph._default_in_grad_kwargs, **in_grad_kwargs)
        intermediate_kwargs = intermediate_kwargs or {}
        intermediate_kwargs = dict(TorchGraph._default_intermediate_kwargs, **intermediate_kwargs)
        
        graph_plot(self.G, self.pos, **graph_kwargs,
                   ax=ax, ax_margins=ax_margins, node_kwargs=node_kwargs,
                   edge_kwargs=edge_kwargs,node_label_kwargs=node_label_kwargs)

        if with_all:
            with_forward = True
            with_backward = True
            with_in_grad = True

        if with_forward:
            draw_edge_labels(self.G, self.pos, self.edge_outputs,
                             **forward_kwargs, ax=ax,
                             ax_margins=ax_margins,
                             label_format_func=num_label_format,
                             format_precision=num_label_precision)

        if with_backward:
            draw_edge_labels(self.G, self.pos, self.edge_grads,
                             **backward_kwargs, ax=ax,
                             ax_margins=ax_margins,
                             label_format_func=num_label_format,
                             format_precision=num_label_precision)
            
        if with_in_grad:
            draw_node_labels(self.G, self.pos, self.input_grads,
                             **in_grad_kwargs, ax=ax,
                             ax_margins=ax_margins,
                             label_format_func=num_label_format,
                             format_precision=num_label_precision)

        if with_intermediate:
            draw_edge_labels(self.G, self.pos, self.intermediate_labels,
                             **intermediate_kwargs, ax=ax,
                             ax_margins=ax_margins,
                             label_format_func=num_label_format,
                             format_precision=num_label_precision)            

        if update_axlims:
            do_axlims_update(ax, ax_margins)

    def save_images(self,
        image_filename_tpl="output/step_{step}.eps",
        *args,
        **kwargs
    ):
        graph_ani = GraphAnimation(graph=self, *args, **kwargs)

        for istep, _ in enumerate(graph_ani()):
            graph_ani.fig.savefig(
                image_filename_tpl.format(step=istep),
                bbox_inches='tight'
            )

    def make_animation(self,
        interval=500,
        *args,
        **kwargs
    ):
        graph_ani = GraphAnimation(graph=self, *args, **kwargs)

        ani = animation.FuncAnimation(
            graph_ani.fig, lambda x: [],
            frames=lambda: graph_ani(), interval=interval
        )
        
        return ani

    def animate(self,
        interval=500,
        *args,
        **kwargs
    ):
        ani = self.make_animation(interval=interval, *args, **kwargs)
        html = HTML(ani.to_jshtml())
        if os.path.exists("None0000000.png"): os.remove("None0000000.png")
        return html

