#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import torch
import inspect

def ext_inner_tensor(a):
    if a is None:
        return None
    elif isinstance(a, tuple):
        return (ext_inner_tensor(it) for it in a)
    elif isinstance(a, list):
        return [ext_inner_tensor(it) for it in a]
    elif isinstance(a, TrackedTensor):
        return a._inner_tensor
    elif not isinstance(a, torch.Tensor):
        t = torch.tensor(a, dtype=torch.float32, requires_grad=True)
        return t._inner_tensor
    else:
        return a

class TrackedTensor(object):
    callback = None
    as_tensor = torch.as_tensor
    
    def __init__(self, data, **kwargs):
        if isinstance(data, TrackedTensor):
            self._inner_tensor = data._inner_tensor
        else:
            self._inner_tensor = TrackedTensor.as_tensor(data, **kwargs)
        
            if not TrackedTensor.callback is None:
                TrackedTensor.callback(self._inner_tensor)

    @classmethod
    def __torch_function__(cls, func, types, args=(), kwargs=None):
        if kwargs is None:
            kwargs = {}
        return tracking_func_wrapper(func, *args, **kwargs)

    def __getattr__(self, name):
        try:
            return getattr(self._inner_tensor, name)
        except KeyError:
            msg = "'{0}' object has no attribute '{1}'"
            raise AttributeError(msg.format(type(self).__name__, name))

def tracking_func_wrapper(func, *args, **kwargs):
    args = (ext_inner_tensor(a) for a in args)
    kwargs = {k: ext_inner_tensor(a) for k, a in kwargs}
    res = func(*args, **kwargs)

    if isinstance(res, tuple):
        res = (TrackedTensor(r)
           if isinstance(r, torch.Tensor)
           else r for r in res)
    elif isinstance(res, list):
        res = [TrackedTensor(r)
               if isinstance(r, torch.Tensor)
               else r for r in res]
    elif isinstance(res, torch.Tensor):
        res = TrackedTensor(res)
        
    return res

class TrackingFuncWrapper:
    def __init__(self, inner_func):
        self.inner_func = inner_func
        
    def __call__(self, *args, **kwargs):
        return tracking_func_wrapper(self.inner_func, *args, **kwargs)

# add the wrapped versions of all the Tensor methods to TrackedTensor
# we do this mainly because __getattr__ does not work with operator overloads
TrackedTensor_dir = dir(TrackedTensor)
for func_name, func in inspect.getmembers(torch.Tensor, predicate=inspect.isroutine):
    if func_name not in TrackedTensor_dir:
        setattr(TrackedTensor, func_name,
                lambda *args, _inner_func=func, **kwargs: tracking_func_wrapper(_inner_func, *args, **kwargs))

def trackable_function(decor_arg):
    decor_arg.apply = TrackingFuncWrapper(decor_arg.apply)
    return decor_arg

class HookedFunction:
    def __init__(self, orig_func, callback):
        self.orig_func = orig_func
        self.callback = callback
        
    def __call__(self, *args, **kwargs):
        ret = self.orig_func(*args, **kwargs)
        return self.callback(ret)
                
class TensorTracker:
    def __init__(self, callback=None, names_callback=None):
        self.orig_callback = None
        self.callback = callback
        self.names_callback = names_callback
    
    def __enter__(self):
        self.orig_callback = TrackedTensor.callback
        TrackedTensor.callback = self.callback
        torch.tensor = HookedFunction(torch.tensor, TrackedTensor)
        torch.as_tensor = HookedFunction(torch.as_tensor, TrackedTensor)
        
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        TrackedTensor.callback = self.orig_callback
        torch.tensor = torch.tensor.orig_func
        torch.as_tensor = torch.as_tensor.orig_func
        
        if not self.names_callback is None:
            # get the locals dict of the enclosing scope
            frame = inspect.currentframe()
        
            try:
                locals_dict = frame.f_back.f_locals
                make_tensor_names(locals_dict, self.names_callback)            

            finally:
                del frame

def make_tensor_names(locals_dict, names_callback):
    for k, v in locals_dict.items():
        if isinstance(v, torch.Tensor) \
        or isinstance(v, TrackedTensor):
            names_callback((k, ext_inner_tensor(v)))

