#!/usr/bin/env python3
# -*- coding: utf-8 -*-

_default_node_name_dict = {
    'AddBackward0': '+',
    'MulBackward0': '*',
    'SubBackward0': '-',
    'SinBackward': 'sin',
    'CosBackward': 'cos',
    'SplitBackward': 'split',
    'SigmoidBackward': '$\sigma$'
}

def register_node_name(original_name, new_name):
    _default_node_name_dict[original_name] = new_name
